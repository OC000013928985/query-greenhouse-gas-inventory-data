# Query Greenhouse Gas Inventory Data


This code is a workaround the non-functional unfccc-di-api 3.0.2 module for Python. 

It downloads, unpacks, reads and reformats a data set from the UNFCCC data base. The resulting data set can be imported in the Mesap data base.
